#include "skip_liste.hpp"

#include <cassert>
#include <random>
#include <iostream>

int main() {

  { //insertion en s'assurant du tri
    SkipListe sl ;

    for(int i = 20; i >= 0; --i) {
      sl.inserer(i) ;
    }
    sl.afficher() ;

    assert(sl.test_tri()) ;
  }

  { //insertion en n'assurant plus le tri
    SkipListe sl ;

    //generateurs aleatoires
    std::default_random_engine rd ;
    std::uniform_int_distribution<int> rand_int(0,99) ;

    for(int i = 20; i >= 0; --i) {
      sl.inserer(rand_int(rd)) ;
    }
    sl.afficher() ;

    assert(sl.test_tri()) ;
    sl.inserer(100);
    sl.inserer(51);
    sl.inserer(0);
    sl.afficher();
    assert(sl.chercher(100) != nullptr);
    assert(sl.chercher(51) != nullptr);
    assert(sl.chercher(0) != nullptr);
    assert(sl.chercher(1000) == nullptr);
    assert(sl.chercher(-1000) == nullptr);

    std::cout<<"Tests passés ! (=°w°=)"<<std::endl;
  }

}
